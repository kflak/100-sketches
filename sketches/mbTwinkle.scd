(
~mbTwinkleFx.free;
~mbTwinkle.do(_.free);
~mbTwinkleFx = FxChain.new(
    fadeInTime: 10,
    level: 0.2,
    fadeOutTime: 10,
    out: ~masterBus,
); 
~mbTwinkleFx.add(\flanger, [
    \feedback, 0.08,
    \depth, 0.04,
    \rate, 0.03,
    \decay, 0.01,
    \mix, 0.3,
]);
~mbTwinkleFx.add(\eq, [ 
    \hishelfdb, -9,
    \hishelffreq, 600,
]);
~mbTwinkleFx.add(\jpverb, [
    \revtime, 3, 
    \mix, 0.3
]);
~mbTwinkleFx.addPar(
    \comb, [\mix, 0.4, \delay, 0.2, \decay, 2],
    \comb, [\mix, 0.4, \delay, 0.3, \decay, 2],
    \comb, [\mix, 0.4, \delay, 0.5, \decay, 2],
); 

~mbTwinkle = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5,
        threshold: 0.03,
        minibeeID: id,
        minAmp: -20,
        maxAmp: 0,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \pm,
                \dur, Prand((0.1..0.4), inf),
                \scale, Scale.minorPentatonic,
                \degree, (0..8).choose,
                \octave, Prand((2..5), inf) + 3.rand,
                \modfreq, Pfunc{|ev| ev.use { ~freq.() + (~freq.()/128).rand }},
                \modfreqDev, Pwhite(10, 400),
                \legato, 1.2,
                \attack, Pkey(\dur) * Pkey(\legato),
                \release, Pkey(\dur),
                \pmindex, Pwhite(1.0, 4.0),
                \ampScale, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \db, Pseg([-70, 0, -70], [3*dt, 3*dt]) 
                + Pkey(\ampScale),
                \pan, Pwhite(-1.0, 1.0), 
                \out, ~mbTwinkleFx.in,
                \group, ~mbTwinkleFx.group,
            ).play;
        };
    )
}
) 

// (
// ~mbTwinkleFx.play;
// ~mbTwinkle.do(_.play);
// )
// (
// ~mbTwinkleFx.free;
// ~mbTwinkle.do(_.free);
// )
