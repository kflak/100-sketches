(
~mbScaryFx = FxChain.new(
    out: ~masterbus, 
    fadeInTime: 10, 
    level: 0.dbamp,
    fadeOutTime: 30
);
~mbScaryFx.add(\comb, [
    \mix, 0.3,
    \delay, Pwhite(0.01, 0.011), Pwhite(4.0, 8.0),
    \decay, 0.3,
    \lag, 8
]);
// ~mbScaryFx.add(\jpverb,[
//     \revtime, Pwhite(0.1, 3), 3,
//     \mix, 0.01,
//     \lag, 3,
// ]);
~mbScaryFx.add(\eq, [
    \locut, 50,
    \hishelfdb, -4,
    \hishelffreq, 600
]);
~mbScaryFx.add(\compressor, [
    \ratio, 6,
    \thresh, -9.dbamp, 
    \amp, 6.dbamp,
]);

~mbScary = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.01,
        minibeeID: id,
        minAmp: -6,
        maxAmp: 0,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(~buf[\verkstedshallen] ++ ~buf[\celloLublin]), 
                \dur, dt.linlin(0.0, 1.0, 1, 2),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \release, Pkey(\dur) * 4,
                \attack, Pkey(\dur), 
                \playbackRate, Pexprand(0.001, 0.002), 
                \posDev, Pexprand(0.0001, 0.01),
                \startPos, Pwhite(0.0, 1.0),
                \grainfreq, Pexprand(160, 240),
                \grainsize, 8 * Pkey(\grainfreq).reciprocal,
                \pan, Pwhite(-1.0, 1.0),
                \panDev, 1,
                \rateDev, Pwhite(0.001, 0.1),
                \legato, 0.8,
                \out, ~mbScaryFx.in,
                \group, ~mbScaryFx.group
            ).play;
        }
    )
}
)

// (
// ~mbScaryFx.play;
// ~mbScary.do(_.play);
// )
// (
// ~mbScaryFx.free;
// ~mbScary.do(_.free);
// )

// s.plotTree
// s.meter;
