(

~kullipoemChoppedProbs = 1 ! ~buf[\kullipoemChopped].size; 

~kullipoemChoppedFx = FxChain.new(
    fadeInTime: 5, 
    level: 0.dbamp, 
    fadeOutTime: 5,
    out: ~masterBus,
); 
~kullipoemChoppedFx.add(\eq, [
    \locut, 60, 
]);
~kullipoemChoppedFx.add(\jpverb, [
    \mix, 0.1,
    \revtime, 2, 
]);

~kullipoemChopped = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.09, 
        minibeeID: id,
        minAmp: -15,
        maxAmp: -6,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \playbuf,
                \buf, Pwrand(~buf[\kullipoemChopped], ~kullipoemChoppedProbs.normalizeSum),
                \dur, Pfunc({|ev| ev.buf.duration * 0.5 }),
                \release, Pkey(\dur) * 2,  
                \loop, 0,
                \legato, 0.8,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \out, ~kullipoemChoppedFx.in,
                \group, ~kullipoemChoppedFx.group,
            ).play;
        }
    );
}; 
)

// (
// ~kullipoemChoppedFx.play;
// ~kullipoemChopped[0..3].do(_.play);
//     // ~kullipoemChopped.do(_.play);
// )
// (
//     ~kullipoemChoppedFx.free;
//     ~kullipoemChopped.do(_.free);
// )
// first minute equal prob
// 1.00-1:40 last questions (what are you focusing on)
// 1:40-2:30 equal probability but dropping off to one sensor only
// 2:30 drums only
// ~kullipoemChoppedProbs = ((0.1) ! 11 )++ ( (0.8) ! 7 );
