(
~runningWaterFx = FxChain.new(
    fadeInTime: 30,
    level: 0.dbamp,
    fadeOutTime: 20,
    // fadeOutTime: 5,
    out: ~masterBus,
    hook: {
        ~runningWater !? {
            ~runningWater.do(_.free);
        }
    }
);
~runningWaterFx.add(\jpverb, [
    \revtime, 1,
    \mix, 0.4,
]);
~runningWaterFx.add(\eq, [
    \locut, 400,
]);

~runningWater = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5,
        threshold: 0.05,
        minibeeID: id,
        minAmp: -18,
        maxAmp: -6,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \playbuf,
                \buf, Prand(~buf[\runningWater]),
                \attack, 0,
                \dur, Pfunc({|ev| ev.buf.duration}),
                \release, 0.2,
                \legato, 1,
                \pan, Pwhite(0.0, 1.0, dt.linlin(0.0, 1.0, 3, 8)),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \out, ~runningWaterFx.in,
                \group, ~runningWaterFx.group,
            ).play;
        };
    );
};

)

// (
//     ~runningWaterFx.play;
//     // ~runningWater[6..7].do(_.play);
//     ~runningWater.do(_.play);
// )
// (
//     ~runningWaterFx.free;
//     // ~runningWater[6..7].do(_.free);
//     ~runningWater.do(_.free);
// )
