( 
    var lag = 20;
    var fxPreset = (); 
    var mbPreset = ();

    fxPreset[\default] =  (
        \fxOut: [
            \fadeInTime : 5,
            \amp :  0.7,
            \fadeOutTime : 5,
        ],
        \jpverb: [
            \mix: 0.0,
            \revtime: 1,
        ],
        \comb: [
            \mix: 0.0,
            \delay: 0.3,
            \decay: 5,
            \amp: 0.333,
        ], 
        \comb1: [
            \mix: 0.0,
            \delay: 0.5,
            \decay: 5,
            \amp: 0.333,
        ],
        \comb2: [
            \mix: 0.0,
            \delay: 0.7,
            \decay: 5,
            \amp: 0.333,
        ],
        \eq: [
            \locut: 120,
            \hishelffreq: 500,
            \hishelfdb: 0,
        ],
        \greyhole: [
            \mix: 0.0,
            \feedback: 0.9,
            \delayTime: 0.2,
        ],
        \flanger: [ 
            \mix: 0.0,
            \feedback: 0.7,
            \depth: 0.06,
            \rate: 0.03,
            \decay: 0.01,
        ],
        \compressor: [
            \ratio: 1,
            \thresh: 0.5,
            \clampTime: 0.01,
            \amp: 1,
        ],
    );

    fxPreset[\reverb] = (
        \fxOut : [
            \fadeInTime: 5,
            \amp: 0.7,
            \fadeOutTime: 5,
        ], 
        \jpverb : [
            \mix: 0.5,
            \revtime: 4,
        ],
        \comb : [
            \mix: 0.0,
            \delay: 0.3,
            \decay: 5,
            \amp: 0.333,
        ],
        \comb1 : [
            \mix: 0.0,
            \delay: 0.5,
            \decay: 5,
            \amp: 0.333,
        ], 
        \comb2 : [
            \mix: 0.0,
            \delay: 0.7,
            \decay: 5,
            \amp: 0.333,
        ],
        \eq : [
            \locut: 20,
            \hishelffreq: 50,
            \hishelfdb: -6,
        ],
        \greyhole : [
            \mix: 0.0,
            \feedback: 0.9,
            \delayTime: 0.2,
        ],
        \flanger: [ 
            \mix: 0.0,
            \feedback: 0.7,
            \depth: 0.06,
            \rate: 0.03,
            \decay: 0.01,
        ],
        \compressor: [
            \ratio: 1,
            \thresh: 0.5,
            \clampTime: 0.01,
            \amp: 1,
        ]
    ); 

    fxPreset[\combLong] = (
        \fxOut : [
            \fadeInTime: 5,
            \amp: 0.7,
            \fadeOutTime: 5,
        ],
        \jpverb : [
            \mix: 0.0,
            \revtime: 4,
        ],
        \comb : [
            \mix: 0.7,
            \delay: 0.3,
            \decay: 5,
            \amp: 0.333,
        ],
        \comb1: [
            \mix: 0.7,
            \delay: 0.5,
            \decay: 5,
            \amp: 0.333,
        ],
        \comb2: [
            \mix: 0.7,
            \delay: 0.7,
            \decay: 5,
            \amp: 0.333,
        ],
        \eq: [
            \locut: 20,
            \hishelffreq: 50,
            \hishelfdb: -6,
        ],
        \greyhole: [
            \mix: 0.0,
            \feedback: 0.9,
            \delayTime: 0.2,
        ],
        \flanger: [ 
            \mix: 0.0,
            \feedback: 0.7,
            \depth: 0.06,
            \rate: 0.03,
            \decay: 0.01,
        ],
        \compressor: [
            \ratio: 1,
            \thresh: 0.5,
            \clampTime: 0.01,
            \amp: 1,
        ]
    );

    fxPreset[\combPsycho] = (
        \fxOut: [
            \fadeInTime: 5,
            \amp: 0.7,
            \fadeOutTime: 5,
        ],
        \jpverb: [
            \mix: 0.0,
            \revtime: 4,
        ],
        \comb: [
            \mix: 0.4,
            \delay: 0.002,
            \decay: 4,
            \amp: 0.05,
        ],
        \comb1: [
            \mix: 0.4,
            \delay: 0.005,
            \decay: 4,
            \amp: 0.25,
        ],
        \comb2: [
            \mix: 0.4,
            \delay: 0.007,
            \decay: 4,
            \amp: 0.333,
        ],
        \eq: [
            \locut: 120,
            \hishelffreq: 600,
            \hishelfdb: -12,
        ],
        \greyhole: [
            \mix: 0.0,
            \feedback: 0.9,
            \delayTime: 0.2,
        ],
        \flanger: [ 
            \mix: 0.0,
            \feedback: 0.7,
            \depth: 0.06,
            \rate: 0.03,
            \decay: 0.01,
        ],
        \compressor: [
            \ratio: 1,
            \thresh: 0.5,
            \clampTime: 0.01,
            \amp: 1,
        ]
    ); 

    fxPreset[\greyholeEternal] = (
        \fxOut: [
            \fadeInTime: 5,
            \amp: 0.7,
            \fadeOutTime: 5,
        ],
        \jpverb: [
            \mix: 0.0,
            \revtime: 4,
        ],
        \comb: [
            \mix: 0.0,
            \delay: 0.002,
            \decay: 4,
            \amp: 0.05,
        ],
        \comb1: [
            \mix: 0.0,
            \delay: 0.005,
            \decay: 4,
            \amp: 0.25,
        ],
        \comb2: [
            \mix: 0.0,
            \delay: 0.007,
            \decay: 4,
            \amp: 0.333,
        ],
        \eq: [
            \locut: 120,
            \hishelffreq: 600,
            \hishelfdb: -12,
        ],
        \greyhole: [
            \mix: 0.6,
            \feedback: 0.9,
            \delayTime: 0.2,
        ],
        \compressor: [
            \ratio: 1,
            \thresh: 0.5,
            \clampTime: 0.01,
            \amp: 1,
        ],
        \flanger: [ 
            \mix: 0.0,
            \feedback: 0.7,
            \depth: 0.06,
            \rate: 0.03,
            \decay: 0.01,
        ]
    );

    fxPreset[\granularBounce] = (
        \fxOut: [
            \fadeInTime: 5,
            \amp: 0.7,
            \fadeOutTime: 5,
        ],
        \jpverb: [
            \mix: 0.0,
            \revtime: 4,
        ],
        \comb: [
            \mix: 0.0,
            \delay: 0.002,
            \decay: 4,
            \amp: 0.05,
        ],
        \comb1: [
            \mix: 0.0,
            \delay: 0.005,
            \decay: 4,
            \amp: 0.25,
        ],
        \comb2: [
            \mix: 0.0,
            \delay: 0.007,
            \decay: 4,
            \amp: 0.333,
        ],
        \eq: [
            \locut: 120,
            \hishelffreq: 600,
            \hishelfdb: -12,
        ],
        \greyhole: [
            \mix: 0.0,
            \feedback: 0.9,
            \delayTime: 0.2,
        ],
        \compressor: [
            \ratio: 4,
            \thresh: 0.5,
            \clampTime: 0.01,
            \amp: 1,
        ],
        \flanger: [ 
            \mix: 0.9,
            \feedback: 0.7,
            \depth: 0.06,
            \rate: 0.03,
            \decay: 0.01,
        ]
    );

    fxPreset[\granularBounceDelay] = (
        \fxOut: [
            \fadeInTime: 5,
            \amp: 0.7,
            \fadeOutTime: 5,
        ],
        \jpverb: [
            \mix: 0.0,
            \revtime: 4,
        ],
        \comb: [
            \mix: 0.7,
            \delay: 0.3,
            \decay: 5,
            \amp: 0.333,
        ],
        \comb1: [
            \mix: 0.7,
            \delay: 0.5,
            \decay: 5,
            \amp: 0.333,
        ],
        \comb2: [
            \mix: 0.7,
            \delay: 0.7,
            \decay: 5,
            \amp: 0.333,
        ],
        \eq: [
            \locut: 120,
            \hishelffreq: 600,
            \hishelfdb: -6,
        ],
        \greyhole: [
            \mix: 0.0,
            \feedback: 0.9,
            \delayTime: 0.2,
        ],
        \compressor: [
            \ratio: 4,
            \thresh: 0.5,
            \clampTime: 0.01,
            \amp: 1,
        ],
        \flanger: [ 
            \mix: 0.9,
            \feedback: 0.7,
            \depth: 0.06,
            \rate: 0.03,
            \decay: 0.01,
        ]
    );

    ~angelAllFx = ~mb.collect ({
        var chain = FxChain.new(out: ~masterBus);
        chain.addPar(\comb, \comb, \comb); 
        chain.add(\compressor); 
        chain.add(\flanger); 
        chain.add(\greyhole);
        chain.add(\jpverb ); 
        chain.add(\eq); 
        chain.lag = lag;
        chain.presetDict = fxPreset; 
    });
    //--------------------------------------------------------- 

    mbPreset[\default] = (
        \speedlim: 0.5,
        \threshold: 0.03,
        \minAmp: -6,
        \maxAmp: 3,
        \grainsize: 0.2,
        \numGrains: 20,
        \rate: 1,
        \legato: 2, 
        \attack: 0.5,
        // \attack: Pwhite(0.3, 0.5)
    ); 

    mbPreset[\granularBounce] = (
        \speedlim: 0.5, 
        \threshold: 0.03,
        \minAmp: -0,
        \maxAmp: 12, 
        \grainsize: 0.2,
        \numGrains: 100,
        \rate: Pwhite(0.5, 1.3),
        \legato: 0.2,
        \attack: 0.5,
        // \attack: Pwhite(0.3, 0.5)
    );

    mbPreset[\granularBounceDelay] = (
        \speedlim: 0.5, 
        \threshold: 0.03,
        \minAmp: -0,
        \maxAmp: 12, 
        \grainsize: 0.2,
        \numGrains: 100,
        \rate: Pwhite(0.5, 1.3),
        \legato: 0.2,
        \attack: 0.5,
        // \attack: Pwhite(0.3, 0.5)
    );

    ~angelAll = ~mb.collect{|id, idx|
        MBDeltaTrig.new(
            speedlim: 0.5, 
            threshold: 0.03,
            minibeeID: id,
            minAmp: -6,
            maxAmp: 3,
            presetDict: mbPreset,
            function: {|dt, minAmp, maxAmp, preset|
                var startPos, buf, numFrames, grainsize, numGrains, 
                step, len, pos, duration, attack, release;
                // buf = ~buf[\rain].choose;
                buf = ~buf[\angelKlli].choose;
                numFrames = buf.numFrames;
                grainsize = preset[\grainsize];
                numGrains = preset[\numGrains];
                step = grainsize * s.sampleRate;
                //in samples, not seconds....
                len = dt.linlin(0.0, 1.0, step, step * numGrains);
                startPos = rrand(0, numFrames - len);
                pos = (startPos, startPos+step..startPos+len); 
                startPos = pos[pos.size-1].mod(numFrames); 
                duration = len/s.sampleRate;
                attack = preset[\attack];
                release = duration - attack;
                Pbind(
                    \instrument, \playbuf,
                    \buf, buf,
                    \dur, grainsize,
                    \attack, attack,
                    \release, Pkey(\dur) * 2,
                    \startPos, Pseq(pos),
                    \legato, preset[\legato],
                    \rate, preset[\rate],
                    \db, dt.linlin(0.0, 1.0, minAmp, maxAmp) 
                    + Pseg([-70, 0, -70], [attack, release]), 
                    \pan, Pwhite(-1.0, 1.0),
                    \out, ~angelAllFx[idx].in,
                    \group, ~angelAllFx[idx].group,
                ).play; 
            }
        );
    }; 

    ~angelAllSelector = ~mb.collect{|id, idx|
        var mbPre = [
            \default,
            \granularBounce,
            \granularBounceDelay,
        ];
        var fxPre = [
            \default,
            \greyholeEternal,
            \reverb,
            \combPsycho,
            \combLong,
            \granularBounce,
            \granularBounceDelay,
        ];
        MBDeltaTrig.new(
            speedlim: 3, 
            threshold: 0.01,
            minibeeID: id,
            function: {|dt|
                var mbIndex = dt.linlin(0.0, 0.2, 0, mbPre.size-1);
                var fxIndex = dt.linlin(0.0, 0.2, 0, fxPre.size-1);

                [mbPre[mbIndex], fxPre[fxIndex]].postln;

                ~angelAll[5].loadPreset(mbPre[mbIndex]);
                ~angelAllFx[5].loadPreset(fxPre[fxIndex]);
            }
        );
    }; 
) 
(
    //     // s.record("/home/kf/sc/100-sketches/videos/audio/angelReverb" ++ Date.getDate.stamp ++".wav");
    fork{
        ~angelAllFx[5].play;
        // ~angelAll.do(_.play);
        s.sync;
        // ~angelAllFx.do(_.loadPreset(\default));
        ~angelAllSelector[2].play;
        ~angelAll[5].loadPreset(\default);
        ~angelAll[5].play;
    }
)

~angelAllFx.do(_.loadPreset(\default));
~angelAllFx.do(_.loadPreset(\greyholeEternal));
~angelAllFx.do(_.loadPreset(\reverb));
~angelAllFx.do(_.loadPreset(\combPsycho));
~angelAllFx.do(_.loadPreset(\combLong));
~angelAllFx.do(_.loadPreset(\granularBounce));
~angelAllFx.do(_.loadPreset(\granularBounceDelay));
~angelAll.do(_.loadPreset(\default));
~angelAll.do(_.loadPreset(\granularBounce));
~angelAll.do(_.loadPreset(\granularBounceDelay));
(
    ~angelAllFx.do(_.free);
    ~angelAll.do(_.free);
    ~angelAllSelector.do(_.free);
    // SystemClock.sched(10, {s.stopRecording});
) 

~angelAllFx[0].fx[\eq].get(\locut, {|i| i.postln})
~angelAllFx[0].fx[\jpverb].get(\mix, {|i| i.postln})
~angelAllFx[0].fx[\greyhole].get(\mix, {|i| i.postln})
~angelAllFx[0].fx[\comb2].get(\delay, {|i| i.postln})
~angelAllFx[0].fx[\fxOut].get(\amp, {|i| i.postln});
~angelAllFx[0].fx[\fxOut].set(\amp, 0.4);
~angelAllFx[0].presetDict;

s.meter;s.plotTree;

[\mix, 0.3, \rev, 0.4].pairsDo{|i, j| [i, j].postln};
~angelSetMBPreset.(\granularBounce, 0);
~angelAll[0].maxAmp;
~buf[\angelKlli];
~angelAll[0].presetDict[\default];
~mb;
