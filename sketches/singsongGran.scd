(
    ~singsongGran = ();
    ~singsongGran[\fx] = FxChain.new(
        fadeInTime:5, 
        level: -12.dbamp, 
        fadeOutTime: 5,  
        in: ~mic[1],  
        out: ~masterBus
    );
    ~singsongGran[\fx].add( \comb, [ 
        \delay, 1, 
        \decay, 6, 
        \mix, 0.3,
        \amp, -18.dbamp, 
    ]);
    ~singsongGran[\fx].addPar(
        \grainIn, [ \grainsize, 0.005, \grainfreq, 15, \mix, 0.9, \amp, -12.dbamp],
        \grainIn, [ \grainsize, 0.05, \grainfreq, 10, \mix, 0.9, \amp, -12.dbamp],
        \grainIn, [ \grainsize, 0.01, \grainfreq, 7, \mix, 0.9, \amp, -12.dbamp],
    );
    ~singsongGran[\fx].add(\jpverb, [
        \revtime, 3,
        \mix, 0.3,
    ]);

    ~singsongGran[\fx].play;
) 

~singsongGran.do(_.free);
