( 

    ~coronaBreathFx = List.new(0);
    2.do{
        ~coronaBreathFx.add(
            FxChain.new(
                fadeInTime: 5,
                level: -12.dbamp,
                fadeOutTime: 5,
                out: ~masterBus,
            );
        );
    };
    ~coronaBreathFx[0].add(\compressor, [
        \ratio, 4,
        \threshold, -12.dbamp,
        \clampTime, 0.005,
    ]);
    ~coronaBreathFx[0].addPar(
        \comb, [\mix, 0.4, \delay, 0.2, \decay, 4, \amp, 1/3],
        \comb, [\mix, 0.4, \delay, 0.5, \decay, 4, \amp, 1/3],
        \comb, [\mix, 0.4, \delay, 0.7, \decay, 4, \amp, 1/3],
    ); 
    ~coronaBreathFx[0].add(\eq, [
        \locut, 120,
        \hishelfdb, -6,
        \hishelffreq, 400,
    ]); 
    ////--------------------------------------------------------- 
    ~coronaBreathFx[1].add(\compressor, [
        \ratio, 4,
        \threshold, -12.dbamp,
    ]);
    ~coronaBreathFx[1].add(\jpverb, [
        \revtime, 2,
        \mix, 0.3,
    ]); 
    ~coronaBreathFx[1].add(\eq, [
        \locut, 120,
        \hishelfdb, -6,
        \hishelffreq, 400,
    ]);
    //--------------------------------------------------------- 
    ~coronaBreathFx[2].add(\greyhole, [
        \delayTime, 0.3,
        \feedback, 0.7,
        \mix, 0.3
    ]); 
    //--------------------------------------------------------- 

    ~coronaBreath = ~mb.collect{|id, idx|
        MBDeltaTrig.new(
            speedlim: 0.5, 
            threshold: 0.02,
            minibeeID: id,
            minAmp: -12,
            maxAmp: -3,
            function: {|dt, minAmp, maxAmp|
                var startPos, currentFx, buf, numFrames, 
                grainsize, numGrains, step, len, pos, rate, duration, 
                attack, release, legato;
                buf = ~buf[\breath].choose;
                numFrames = buf.numFrames;
                grainsize = 0.2;
                numGrains = 20;
                step = grainsize * s.sampleRate;
                ////in samples, not seconds....
                len = dt.linlin(0.0, 1.0, step, step * numGrains);
                startPos = 0;
                pos = (startPos, (startPos+step)..(startPos+len)); 
                pos = pos.select({|i| i < numFrames});

                rate = 1;
                attack = 0.2;
                release = 0.5;
                // duration = len/s.sampleRate;
                // release = duration - attack;
                // legato = 1;
                // legato = 2;

                Pbind(
                    \instrument, \playbuf,
                    \buf, buf,
                    \dur, grainsize,
                    \attack, Pkey(\dur),
                    \release, Pkey(\dur) * 2,
                    \startPos, Pseq(pos),
                    \legato, 1,
                    \rate, Pwhite(0.99, 1.01),
                    // \db, Pseg([-70, 0, -70], [attack, release]), 
                    \db, -6,
                    \pan, Pwhite(-1.0, 1.0),
                    \out, Pfunc({
                        currentFx = ~coronaBreathFx.choose;
                        currentFx.in
                    }),
                    \group, Pfunc({ currentFx.group }),
                ).play; 
            }
        );
    }; 
) 

(
    // s.record("/home/kf/sc/100-sketches/videos/audio/coronaBreath" ++ Date.getDate.stamp ++".wav");
    ~coronaBreathFx.do(_.play);
    ~coronaBreath.do(_.play);
)
(
    ~coronaBreathFx.do(_.free);
    ~coronaBreath.do(_.free);
    // SystemClock.sched(10, {s.stopRecording});
)

// s.plotTree
// ~buf[\coronaWhisper];
