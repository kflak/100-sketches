(
~coronaWhisperGrainFx = List.new(0);
2.do{
    ~coronaWhisperGrainFx.add(
        FxChain.new(
            fadeInTime: 5,
            level: -22.dbamp,
            fadeOutTime: 30,
            out: ~masterBus,
            hook: {
                ~coronaWhisperGrain !? {
                    ~coronaWhisperGrain.do(_.free);
                    ~coronaWhisperGrain = nil;
                }
            }
        )
    )
};
~coronaWhisperGrainFx[0].add(\compressor, [
    \ratio, 4,
    \threshold, -12.dbamp,
]);
~coronaWhisperGrainFx[0].addPar(
    \comb, [\mix, 0.4, \delay, 0.2, \decay, 4, \amp, 1/3],
    \comb, [\mix, 0.4, \delay, 0.5, \decay, 4, \amp, 1/3],
    \comb, [\mix, 0.4, \delay, 0.7, \decay, 4, \amp, 1/3],
);
~coronaWhisperGrainFx[0].add(\eq, [
    \locut, 120,
    \hishelfdb, -6,
    \hishelffreq, 400,
]);
////---------------------------------------------------------
~coronaWhisperGrainFx[1].add(\compressor, [
    \ratio, 4,
    \threshold, -12.dbamp,
]);
~coronaWhisperGrainFx[1].add(\jpverb, [
    \revtime, 2,
    \mix, 0.3,
]);
~coronaWhisperGrainFx[1].add(\eq, [
    \locut, 120,
    \hishelfdb, -6,
    \hishelffreq, 400,
]);
//---------------------------------------------------------
~coronaWhisperGrainFx[2].add(\greyhole, [
    \delayTime, 0.3,
    \feedback, 0.7,
    \mix, 0.3
]);
//---------------------------------------------------------

~coronaWhisperGrain = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5,
        threshold: 0.02,
        minibeeID: id,
        minAmp: -12,
        maxAmp: -3,
        function: {|dt, minAmp, maxAmp|
            var startPos, currentFx, buf, numFrames,
            grainsize, numGrains, step, len, pos, rate, duration,
            attack, release, legato;
            buf = ~buf[\coronaWhisper].choose;
            numFrames = buf.numFrames;
            grainsize = 0.2;
            numGrains = 20;
            step = grainsize * s.sampleRate;
            ////in samples, not seconds....
            len = dt.linlin(0.0, 1.0, step, step * numGrains);
            startPos = 0;
            pos = (startPos, (startPos+step)..(startPos+len));
            pos = pos.select({|i| i < numFrames});
            rate = 1;
            attack = 0.2;
            release = 0.5;

            Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, grainsize,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 2,
                \startPos, Pseq(pos),
                \legato, 1,
                \rate, Pwhite(0.99, 1.01),
                // \db, Pseg([-70, 0, -70], [attack, release]),
                \db, -6,
                \pan, Pwhite(-1.0, 1.0),
                \out, Pfunc({
                    currentFx = ~coronaWhisperGrainFx.choose;
                    currentFx.in
                }),
                \group, Pfunc({ currentFx.group }),
            ).play;
        }
    );
};
)

// (
//     ~coronaWhisperGrainFx.do(_.play);
//     ~coronaWhisperGrain.do(_.play);
// )
// (
    // ~coronaWhisperGrainFx.do(_.free);
//     ~coronaWhisperGrain.do(_.free);
// )
