(
~mbFMFBFx = FxChain.new(
    fadeInTime: 30, 
    level: -9.dbamp, 
    fadeOutTime: 10,
    out: ~masterBus,
    hook: {
        ~mbFMFB !? {
            ~mbFMFB.do(_.free);
        }
    }
); 
~mbFMFBFx.add(\eq, [
    \locut, 60, 
]);
~mbFMFBFx.add(\jpverb, [
    \mix, 0.4,
    \revtime, 3, 
]);
~mbFMFBFx.add(\flanger, [
    \feedback, 0.08,
    \depth, 0.04,
    \rate, 0.03,
    \decay, 0.01,
    \mix, 0.3,
]);

~mbFMFB = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.05, 
        minibeeID: id,
        minAmp: -34,
        maxAmp: -9,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \pmFB,
                \dur, dt.linlin(0.0, 1.0, 0.1, 2),
                \octave, Pwhite(2, 5, 1),
                \degree, 1,
                \modfreq, Pfunc{|ev| ev.use { ~freq.() + rrand(-10.0, 10.0) }},
                \modFeedback, Pwhite(0.01, 1.0),
                \attack, Pwhite(2.0, 7),
                \modattack, Pkey(\attack) * 2,
                \release, dt.linlin(0.0, 1.0, 2.5, 15),  
                \modrelease, Pkey(\release),
                \pmindex, Pwhite(7, 13),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \out, ~mbFMFBFx.in,
                \group, ~mbFMFBFx.group,
            ).play; 
        }
    );
}
)

// (
//     ~mbFMFBFx.play;
//     ~mbFMFB.do(_.play);
// )
// (
//     ~mbFMFBFx.free;
//     ~mbFMFB.do(_.free);
// )
