(
fork{
    ~mbTestFx = FxChain.new(
        fadeInTime: 30,
        level: -18.dbamp,
        fadeOutTime: 5,
        out: ~masterBus,
        hook: {
            ~mbTest !? {
                ~mbTest.do(_.free);
            }
        }
    );

    ~mbTestFx.add(\jpverb, [
        \revtime, 1,
        \mix, 0.4,
    ]);

    ~mbTest = ~mb.collect{|id, idx|
        MBDeltaTrig.new(
            speedlim: 0.5,
            threshold: 0.05,
            minibeeID: id,
            minAmp: -18,
            maxAmp: -6,
            function: {|dt, minAmp, maxAmp|
                Pbind(
                    \instrument, \sine,
                    \attack, 0,
                    \scale, Scale.minorPentatonic,
                    \octave, 3,
                    \degree, idx,
                    \dur, 1,
                    \release, 1,
                    \pan, Pwhite(-1.0, 1.0, 1),
                    \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                    \out, ~mbTestFx.in,
                    \group, ~mbTestFx.group,
                ).play;
            };
        );
    };

    1.wait;

    ~mbTestFx.play;
    ~mbTest.do(_.play);
})

// (
//     ~mbTestFx.play;
//     ~mbTest.do(_.play);
// )
// (
//     ~mbTestFx.free;
//     ~mbTest.do(_.free);
// )

