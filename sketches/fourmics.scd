(
    ~fourmics = {|fadeInTime=10, level=1, fadeOutTime=10| 

        var rev, rev2, rev2bus, comb, pitchUp, pitchDown;
        var lfo;
        var lfo1, lfo1Bus;
        var group, instr, fx;
        var out, outBus;
        var flanger;

        s.makeBundle(nil, {
            group = Group.new();
            fx = Group.tail(group);
            instr = Group.head(group);

            outBus = Bus.audio(s, ~numSpeakers);
            out = Synth(\route, [
                \in, outBus,
                \out, ~masterBus,
                \attack, fadeInTime,
                \release, fadeOutTime,
                \amp, level,
            ], fx);

            rev2bus = Bus.audio(s, ~numSpeakers);
            rev2 = Synth(\jpverb, [
                \in, rev2bus,
                \out, outBus,
                \mix, 0.2,
                \revtime, 4,
            ], fx);

            lfo1Bus = Bus.audio(s, ~numSpeakers);
            lfo1 = Synth(\lfo, [
                \in, lfo1Bus,
                \out, outBus,
                \hi, 2,
                \freq, 6, 
            ], fx);

            // lfoBus = Bus.audio(s, ~numSpeakers);
            // lfo = Synth(\lfo, [
            //     \in, lfoBus,
            //     \out, outBus,
            //     \hi, 2,
            //     \freq, 4, 
            // ], fx);

            pitchUp = Synth(\pitchshift, [
                \in, ~mic[4],
                \out, lfo1Bus,
                \amp, -9.dbamp,
                \pitchRatio, 2,
            ], instr);

            flanger = Synth(\flanger, [
                \in, ~mic[5],
                \out, rev2bus,
                \feedback, 0.08,
                \depth, 0.04,
                \rate, 0.03,
                \decay, 0.01,
                \mix, 1,
            ], instr);

            comb = [0.2, 0.5, 0.75, 0.9].collect({|i, idx|
                Synth(\combMonoIn, [
                    \in, ~mic[6],
                    \out, rev2bus,
                    \delay, i,
                    \amp, idx.linlin(0, 3, -12, -24).dbamp,
                    \decay, 2,
                    \pan, idx.linlin(0, 3, -1.0, 1.0),
                    \mix, 0.5,
                ], instr); 
            });

            pitchDown = [-12, -7, -4].midiratio.collect({|i|
                Synth(\pitchshift, [
                    \in, ~mic[7],
                    \out, outBus,
                    \pitchRatio, i,
                    \amp, -12.dbamp,
                ], instr);
            });
        });

        ~free[\fourmics] = {
            fork{
                // [rev, rev2, pitchUp, pitchDown].do(_.set(\gate, 0));
                out.set(\gate, 0);
                5.wait;
                [lfo1Bus, rev2bus].do(_.free);
                group.free;
            }
        };
    }
)

// ~free[\fourmics].();
// s.plotTree
