(
    // MIDIdef.all;
    var notes = [0, 2, 3, 5, 7, 9, 10, 12];
    // var srcID = MIDIClient.sources.indexOf("launchPad");
    // srcID.postln;
    var group<F2>
    var out, outBus;

    outBus = Bus.audio(s, ~numSpeakers);
    out = Synth(\route, [
        \in, outBus,
        \out, ~masterBus,
        \attack, fadeInTime,
        \releaseease, fadeOutTime,
    ]);

    ~lpsynth = nil ! 127;

    //clear launchPad
    ~launchPadOut.control(176, 0, 0);

    //set all active pads
    128.do{|i|
        if((i%16) < 8) {
            case
            {i < 8} {~launchPadOut.noteOn(0, i, ~launchPadColor[\amberLo])}
            {i < 24} {~launchPadOut.noteOn(0, i, ~launchPadColor[\greenLo])}
            {i < 40} {~launchPadOut.noteOn(0, i, ~launchPadColor[\redLo])}
            {i < 56} {~launchPadOut.noteOn(0, i, ~launchPadColor[\amberLo])}
            {i < 72} {~launchPadOut.noteOn(0, i, ~launchPadColor[\greenLo])}
            {i < 88} {~launchPadOut.noteOn(0, i, ~launchPadColor[\redLo])}
            {i < 104} {~launchPadOut.noteOn(0, i, ~launchPadColor[\amberLo])}
            {i < 120} {~launchPadOut.noteOn(0, i, ~launchPadColor[\greenLo])}
        };
    };

    //top row, bass
    MIDIdef.noteOn(\launchPadBass, {
        arg val, note;
        var n = notes[note];
        var freq = (n + 30).midicps;
        var attack = 2, rel = 5;
        ~lpsynth[note] = Synth(\pm,
            [
                \freq, freq,
                \modfreq, freq ,
                \pan, rrand(0.0, 1.0),
                \pmindex, 40,
                \out, outBus,
                \attack, attack,
                \release, rel,
                \modattack, attack * 2,
                \modrelease, rel,
                \amp, -26.dbamp,
                \amfreq, n + 1,
                \amdepth, 2,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\amberhi])       
    }, noteNum: (0..7), chan: 0);

    MIDIdef.noteOff(\launchPadBassOff, {
        arg val, note, channel, src;
        ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\amberlo])
    }, noteNum: (0..7), chan: 0);

    //second row, fm
    MIDIdef.noteOn(\launchPadFM, {
        arg val, note, channel, src;
        var offset = 54;
        var n = notes[note - 16] + offset;
        var freq = n.midicps;
        var attack = 5, rel = 0.5;
        ~lpsynth[note] = Synth(\pm,
            [
                \freq, freq,
                \modfreq, freq * rrand(0.9, 1.4),
                \pan, rrand(0.0, 1.0),
                \pmindex, 30,
                \out, outBus,
                \attack, attack,
                \release, rel,
                \modattack, attack,
                \modrelease, rel,
                \amp, -35.dbamp,
                \amfreq, 120,
                \amdepth, 8,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\greenhi])
    }, noteNum: (16..23), chan: 0);

    MIDIdef.noteOff(\launchPadFMOff, {
        arg val, note, channel, src;
        ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\greenlo])
    }, noteNum: (16..23), chan: 0);

    //third row, fm grains
    MIDIdef.noteOn(\launchPadFMGrains, {
        arg val, note, channel, src;
        var offset = 54;
        var n = notes[note - 32] + offset;
        var freq = note.explin(31, 40, 100, 2000);
        var attack = 0.1, rel = 1;
        ~lpsynth[note] = Synth(\fmgrainDustGate,
            [
                \carfreq, freq,
                \modfreq, freq * rrand(0.5, 1.4),
                \density, n, 
                \grainsize, exprand(0.0005, 0.1),
                \pan, rrand(0.0, 1.0),
                \moddepth, 900,
                \out, outBus,
                \attack, attack,
                \release, rel,
                \amp, -26.dbamp,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\redhi])       
    }, noteNum: (32..39), chan: 0);

    MIDIdef.noteOff(\launchPadFMGrainsOff, {
        arg val, note, channel, src;
        ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\redlo])
    }, noteNum: (32..39), chan: 0);

    //fourth row, sine grains
    MIDIdef.noteOn(\launchPadSineGrains, {
        arg val, note, channel, src;
        var offset = 54;
        var n = notes[note - 48] + offset;
        var freq = note.explin(48, 56, 800, 2000);
        var attack = 0.1, rel = 0.1;
        ~lpsynth[note] = Synth(\sineGrainDustGate,
            [
                \freq, freq * rrand(0.9, 1.1),
                \density, n, 
                \grainsize, exprand(0.05, 0.5),
                \pan, rrand(0.0, 1.0),
                \out, outBus,
                \attack, attack,
                \release, rel,
                \amp, -18.dbamp,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\amberhi])       
    }, noteNum: (48..55), chan: 0);

    MIDIdef.noteOff(\launchPadSineGrainsOff, {
        arg val, note, channel, src;
        ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\amberlo])
    }, noteNum: (48..55), chan: 0);

    //fifth row, stoveperc
    MIDIdef.noteOn(\launchPadStoveperc, {
        arg val, note, channel, src;
        var attack = 0, rel = 0.1;
        ~lpsynth[note] = Synth(\clean,
            [
                \buf, ~buf[\stoveperc].choose,
                \pan, rrand(0.0, 1.0),
                \out, outBus,
                \attack, attack,
                \release, rel,
                \amp, -18.dbamp,
                \hipassfreq, 200,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\greenhi])       
    }, noteNum: (64..71), chan: 0);

    MIDIdef.noteOff(\launchPadStovepercOff, {
        arg val, note, channel, src;
        // ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\greenlo])
    }, noteNum: (64..71), chan: 0);

    //sixth row, treeperc
    MIDIdef.noteOn(\launchPadTreeperc, {
        arg val, note, channel, src;
        var attack = 0, rel = 0.1;
        ~lpsynth[note] = Synth(\clean,
            [
                \buf, ~buf[\treeperc].choose,
                \pan, rrand(0.0, 1.0),
                \out, outBus,
                \attack, attack,
                \release, rel,
                \amp, -12.dbamp,
                \hipassfreq, 200,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\redhi])       
    }, noteNum: (80..87), chan: 0);

    MIDIdef.noteOff(\launchPadTreepercOff, {
        arg val, note, channel, src;
        // ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\redlo])
    }, noteNum: (80..87), chan: 0);

    //seventh row, broken piano
    MIDIdef.noteOn(\launchPadBrokenPiano, {
        arg val, note, channel, src;
        var attack = 0;
        var buf = ~buf[\brpiano].choose;
        var rel = buf.duration;
        ~lpsynth[note] = Synth(\clean,
            [
                \buf, buf,
                \pan, rrand(0.0, 1.0),
                \out, outBus,
                \attack, attack,
                \release, rel,
                \amp, -14.dbamp,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\amberhi])       
    }, noteNum: (96..103), chan: 0);

    MIDIdef.noteOff(\launchPadBrokenPianoOff, {
        arg val, note, channel, src;
        // ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\amberlo])
    }, noteNum: (96..103), chan: 0);

    //eight row, cello
    MIDIdef.noteOn(\launchPadCello, {
        arg val, note, channel, src;
        var attack = 0;
        var buf = ~buf[\cello].choose;
        var rel = buf.duration / 2;
        ~lpsynth[note] = Synth(\clean,
            [
                \buf, buf,
                \pan, rrand(0.0, 1.0),
                \out, outBus,
                \attack, attack,
                \release, rel,
                \amp, -10.dbamp,
                \hipassfreq, 200,
            ], target: ~group[\instr];
        );
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\greenhi])       
    }, noteNum: (112..119), chan: 0);

    MIDIdef.noteOff(\launchPadCelloOff, {
        arg val, note, channel, src;
        // ~lpsynth[note].set(\gate, 0);
        ~lpsynth[note] = nil;
        ~launchPadOut.noteOn(0, note, ~launchPadColor[\greenlo])
    }, noteNum: (112..119), chan: 0);
)
