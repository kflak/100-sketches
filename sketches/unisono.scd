(
    fork {     
        var group = Group.new;
        var instr = Group.head(group);
        var fx = Group.after(instr);
        var patterns, pseq, ppar;
        var lead, lead2; 
        var treeperc, treepercRoute, treepercBus, treepercBufs; 
        var bubbles, bubblesRoute, bubblesBus, bubbleBufs;
        var revealControl;
        var bass;
        var kick;
        var reverb, revBus, revMix=0.1;
        var out = Bus.audio(s, ~numSpeakers);

        ~free[\unisono].();
        // ~freeReveal.();
        "sketches/revealTracks.scd".postln;

        treepercBufs = BufArray(~root+/+"audio/treeperc/");
        bubbleBufs = BufArray(~root+/+"audio/bubbles/");

        s.sync;

        revBus = Bus.audio(s, ~numSpeakers);
        reverb = Synth(\jpverb, [\in, revBus, \out, ~bus[\revealTracks], \revtime, 1, \mix, revMix], fx);

        bass = Pbind(
            \instrument, \pm,
            \pmindex, 0.5, 
            \octave, 3,
            \scale, Scale.minor,
            \degree, Pseq((3..0)++[3, 2, -1, 0], inf),
            \dur, 2/4,
            \modfreq, Pkey(\freq),
            \db, -30,
            \out, revBus,
            \group, instr,
        );

        treeperc = Pbind(
            \instrument, \playbuf,
            \buf, Prand(treepercBufs, inf),
            \attack, 0,
            \dur, 1/8,
            \release, 0.2,
            \legato, 0.1,
            \db, Pwhite(-10, -6),
            // \amp, Pfunc{ ~mb[10].movingAverage },
            \out, revBus,
            \group, instr,
        );

        lead = Pbind(
            \instrument, \pm, 
            \pmindex, 0.8,
            \octave, 5,
            \scale, Scale.minor,
            \degree, Pseq((3!16)++(4!16)++(5!16)++(6!8)++(7!8), inf),
            \dur, 1/4,
            \release, Pkey(\dur),
            \legato, 0.3,
            \db, Pwhite(-24, -16),
            // \amp, Pfunc{ ~mb[~mb.indexOf(9)].movingAverage },
            \pan, Pwhite(-1.0, 1.0), 
            \out, revBus,
            \group, instr,
        );

        lead2 = Pbind(
            \instrument, \pm, 
            \pmindex, 0.7,
            \octave, 6,
            \scale, Scale.minor,
            \degree, Pseq((5!16)++(6!16)++(7!16)++(3!8)++(4!4)++(2!4), inf),
            \dur, 1/4,
            \release, Pkey(\dur),
            \legato, 0.3,
            \db, Pwhite(-40, -30),
            // \amp, Pfunc{ ~mb[~mb.indexOf(9)].movingAverage },
            \pan, Pwhite(-1.0, 1.0),
            \modfreq, Pkey(\freq),
            \out, revBus,
            \group, instr,
        );

        kick = Pbind(
            \instrument, \sine,
            \freq, 80,
            \dur, Pseq([3/4, 3/4, 2/4], inf),
            \release, 0.1,
            \attack, 0,
            \legato, 0.1,
            \db, Pwhite(-16, -10),
            \pan, Pwhite(-1.0, 1.0),
            \out, revBus,
            \group, instr,
        );

        bubbles = Pbind(
            \instrument, \playbuf,
            \loop, 0,
            \buf, Prand(bubbleBufs, inf),
            // \dur, Pfunc({|ev| ev.buf.postln; ev.buf.duration }),
            \dur, 1/8,
            \legato, 0.1,
            \pan, Pwhite(-1.0, 1.0),
            \out, revBus,
            \db, 6,
            \group, instr,
        );

        patterns = [bass, treeperc, lead, lead2, kick, bubbles].powerset.reject({|i| i.size < 2}).scramble;
        ppar = patterns.collect{|i| Ppar(i)};
        pseq = Pseq([Pfindur(2, Prand(ppar))], inf).play;

        ~free[\unisono] = {
            fork {
                pseq.stop;
                group.set(\gate, 0);
                5.wait;
                revBus.free; 
                treepercBufs.free;
                bubbleBufs.free;
                group.free;
            }
        }
    }
)

~free[\unisono].();
s.plotTree;
