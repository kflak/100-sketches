(
var mbTrig, bufs;
var eq, eqBus;

eqBus = Bus.audio(s, ~numSpeakers);
eq = Synth(\eq, [\in, eqBus, \locut, 200, \out, ~bus[\mbTalkulator]]);

bufs = BufArray(~root+/+"audio/storychop/");

~free[\mbTalkulator].();

mbTrig = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.05, 
        minibeeID: id,
        minAmp: -6,
        maxAmp: 0,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \playbuf,
                \buf, Prand(bufs),
                // \startPos, Pfunc{|ev| [0, ev.buf.duration].choose },
                // \endPos, Pfunc{|ev| if(ev.startPos == 0){ev.buf.duration}{0} },
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,  
                \rate, Prand([-1, 1]),
                // \playbackRate, 1,
                // \grainsize, 0.2,
                // \grainfreq, 10,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \out, eqBus,
            ).play;
        }
    );
}; 

~play[\mbTalkulator] = {|i|
    case 
    {i.isInteger}{
        mbTrig[~mb.indexOf(i)].play;
    }
    {i.isCollection}{
        i.do{|item|
            mbTrig[~mb.indexOf(item)].play;
        }
    };
};

~stop[\mbTalkulator] = {|i|
    case
    {i.isInteger}{
        mbTrig[~mb.indexOf(i)].stop;
    }
    {i.isCollection}{
        i.do{|item|
            mbTrig[~mb.indexOf(item)].stop;
        }
    };
};

~free[\mbTalkulator] = {
    mbTrig.do(_.free);
    bufs.do(_.free);
};
)

// ~play[\mbTalkulator].(~mb);
// ~stop[\mbTalkulator].(~mb);
