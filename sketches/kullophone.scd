( 
~kullophoneFx = FxChain.new(
    fadeInTime: 10,
    level: 3.dbamp,
    fadeOutTime: 15,
    out: ~masterBus
); 
~kullophoneFx.add(\eq, [
    \hishelfdb, -9,
    \locut, 150
]); 
~kullophoneFx.add(\flanger, [
    \feedback, 0.08,
    \depth, 0.04,
    \rate, 0.03,
    \decay, 0.01,
    \mix, 0.2, 
]); 
~kullophoneFx.add(\jpverb, [
    \mix, 0.2,
    \revtime, 3
]);

~kullophone = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.035, 
        minibeeID: id,
        minAmp: -12,
        maxAmp: 6,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(~buf[\kullophone]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 1),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 4,  
                \rate, Prand([0.25, 1], inf),
                \rateDev, Pwhite(0.0, 0.15),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.001, 0.15),
                \grainfreq, Pwhite(1, 12),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, ~kullophoneFx.in,
                \group, ~kullophoneFx.group,
            ).play;
        }
    );
}; 
)

// (
// ~kullophoneFx.play;
// ~kullophone[4..6].do(_.play);
// // ~kullophone.do(_.play);
// )
// (
// ~kullophoneFx.free;
// ~kullophone.do(_.free);
// )
