(
// var revLine = 
~coughFx = List.new(0); 

5.do{
    ~coughFx.add(
        FxChain.new(
            fadeInTime: 5,
            level: 0.dbamp,
            fadeOutTime: 5,
            out: ~masterBus,
        );
    );
};
~coughFx[0].addPar(
    \comb, [\mix, 0.1, \delay, 0.2, \decay, 1, \amp, 1/3],
    \comb, [\mix, 0.1, \delay, 0.5, \decay, 1, \amp, 1/3],
    \comb, [\mix, 0.1, \delay, 0.7, \decay, 1, \amp, 1/3],
); 
~coughFx[0].add(\eq, [
    \locut, 120,
    \hishelfdb, -6, 
]); 
//--------------------------------------------------------- 
~coughFx[1].add(\jpverb, [
    \revtime, 2,
    \mix, 0.1
]); 
~coughFx[1].add(\eq, [
    \locut, 120,
    \hishelfdb, -6, 
]); 
//--------------------------------------------------------- 
~coughFx[2].add(\greyhole, [
    \delayTime, 0.3,
    \feedback, 0.3,
    \mix, 0.1
]); 
~coughFx[2].add(\eq, [
    \locut, 300,
    \hishelfdb, -6, 
]); 
//--------------------------------------------------------- 
~coughFx[3].add(\eq, [
    \locut, 300,
    \hishelfdb, -6, 
]); 
~coughFx[4].add(\flanger, [
    \feedback, 0.08,
    \depth, 0.04,
    \rate, 0.03,
    \decay, 0.01,
    \mix, 0.2, 
]);

//--------------------------------------------------------- 
~cough = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.07, 
        minibeeID: id,
        minAmp: -20,
        maxAmp: -0,
        function: {|dt, minAmp, maxAmp| 
            var currentFx;
            Pbind(
                \instrument, \playbuf,
                \buf, Prand(~buf[\cough]),
                \dur, 1,
                \release, Pkey(\dur) * 2,  
                \loop, 0,
                \legato, 0.1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \out, Pfunc({
                    currentFx = ~coughFx.choose;
                    currentFx.in
                }),
                \group, Pfunc({ 
                    currentFx.group
                }),
            ).play;
        }
    );
}; 
) 

// (
// ~coughFx.do(_.play);
// ~cough.do(_.play);
// )
// (
// ~coughFx.do(_.free); 
// ~cough.do(_.free);
// ) 
