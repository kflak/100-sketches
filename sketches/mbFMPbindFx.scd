(

    var mbTrig;

    ~free[\mbFMPbindfx].();

    // mbTrig = [16].collect{|id, idx|
    mbTrig = ~mb.collect{|id, idx|
        MBDeltaTrig.new(
            speedlim: 0.5, 
            threshold: 0.05, 
            minibeeID: id,
            minAmp: -32.dbamp,
            maxAmp: -12.dbamp,
            function: {|dt, minAmp, maxAmp|
                PbindFx([
                    \instrument, \pm,
                    \dur, dt.linlin(0.0, 1.0, 0.1, 0.5),
                    \scale, Scale.minorPentatonic,
                    // \freq, Pexprand(160, 440, 1),
                    \degree, Pwhite(0, 24, 1),
                    \modfreq, Pfunc{|ev| ev.use { ~freq.() }},
                    // \modfreq, Pkey(\freq),
                    \attack, Pwhite(2.0, 5),
                    \modattack, Pkey(\attack) * 2,
                    \release, dt.linlin(0.0, 1.0, 2.5, 6),  
                    \modrelease, Pkey(\release),
                    \pmindex, Pwhite(0, 10),
                    \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                    \pan, Pwhite(-1.0, 1.0),
                    \fxOrder, (1..4),
                ],[
                    \fx, \jpverb,
                    \revtime, Pwhite(1, 3),
                    \mix, Pwhite(0.0, 0.2),
                    \cleanupDelay, Pkey(\revtime) + 2
                ],[
                    \fx, \compressor,
                    \ratio, 4,
                    \db, 3,
                    \thresh, -6.dbamp,
                    \cleanupDelay, 8,
                ],[
                    \fx, \eq,
                    \locut, Pexprand(80, 200),
                    \hishelfdb, Pwhite(-24, -16),
                    \cleanupDelay, 8,
                ],[
                    \fx, \limiter, 
                    \limit, -1.dbamp,
                    \cleanupDelay, 8,
                ]).play;
            };
        );
    }; 

    ~mbFMPbindfxPlay = {|i|
        if(i.isInteger){
            mbTrig[~mb.indexOf(i)].play;
        };
        if(i.isCollection){
            i.do{|item|
                mbTrig[~mb.indexOf(item)].play;
            }
        };
    };

    ~mbFMPbindfxStop = {|i|
        if(i.isInteger){
            mbTrig[~mb.indexOf(i)].stop;
        };
        if(i.isCollection){
            i.do{|item|
                mbTrig[~mb.indexOf(item)].stop;
            }
        };
    };

    ~free[\mbFMPbindfx] = {
        mbTrig.do(_.free);
    };
)


// ~free[\mbFMPbindfx].();
// s.plotTree;

