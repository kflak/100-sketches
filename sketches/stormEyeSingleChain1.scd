(
fork{ 
    ~stormEyeSingleChain1Fx = FxChain.new(
        level: 9.dbamp,
        in: ~mic[7],
        out: ~masterBus,
        fadeInTime: 30,
        fadeOutTime: 60,
        numInputBusChannels: 1,
    );

    ~stormEyeSingleChain1Fx.add(\panner, [
        \pan, 0,
    ]);

    ~stormEyeSingleChain1Fx.add(\ringMod, [
        \mix, 0.3,
        \depth, 2,
        \modfreq, 70,
        \amp, 0.dbamp,
    ]); 

    ~stormEyeSingleChain1Fx.add(\ringMod, [
        \mix, 0.3,
        \depth, 2,
        \modfreq, 30,
        \amp, 0.dbamp,
    ]); 
    ~stormEyeSingleChain1Fx.add(\ringMod, [
        \mix, 0.3,
        \depth, 3,
        \modfreq, 40,
        \amp, 0.dbamp,
    ]); 
    ~stormEyeSingleChain1Fx.add(\limiter, [
        \limit, -3.dbamp
    ]); 
    ~stormEyeSingleChain1Fx.add(\jpverb, [
        \revtime, 3,
        \mix, 0.3
    ]); 
    ~stormEyeSingleChain1Fx.add(\ringMod, [
        \mix, 0.3,
        \depth, 2,
        \modfreq, 8,
        \amp, -0.dbamp,
    ]); 
    ~stormEyeSingleChain1Fx.add(\eq, [
        \locut, 500,
        \hishelfdb, -12, 
        \hishelffreq, 700,
        \peakfreq, 450,
        \peakdb, -6,
        \hicut, 900
    ]); 
    ~stormEyeSingleChain1Fx.add(\limiter, [
        \limit, -3.dbamp
    ]); 

    1.wait;
    ~stormEyeSingleChain1Fx.play; 
}) 

// ~stormEyeSingleChain1Fx.free;
