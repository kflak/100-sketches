(
~velcrolatorFx = FxChain.new(
    fadeInTime: 10, 
    level: 8.dbamp, 
    fadeOutTime: 10, 
    out: ~masterBus
); 
~velcrolatorFx.add(\jpverb, [
    \revtime, 3,
    \mix, Pwhite(0.0, 0.1), Pwhite(2, 4),
    \lag, 2,
]);
~velcrolatorFx.add( \compressor, [
    \thresh, -4.dbamp,
    \ratio, 8,
    \amp, 6.dbamp,
]); 

~velcrolator = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.07,
        minibeeID: id,
        minAmp: -6,
        maxAmp: 6,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(~buf[\velcro]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,  
                \rate, 1,
                // \rate, Prand([0.25, 1], inf),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 40),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, ~velcrolatorFx.in,
                \group, ~velcrolatorFx.group,
            ).play;
        }
    );
}; 
);

// (
//     ~velcrolatorFx.play;
//     ~velcrolator.do(_.play)
// ) 
// (
//     ~velcrolatorFx.free;
//     ~velcrolator.do(_.free);
// ); 
