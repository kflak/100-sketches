(
    fork{
        var group = Group.new();
        var instr = Group.head(group);
        var fx = Group.after(instr);
        var route = Group. after(fx);
        var synth, buf, aBus, reverb, dry;

        "sketches/aiuaiu.scd".postln;

        aBus = Bus.audio(s, ~numSpeakers);
        buf = Buffer.read(s, ~root+/+"audio/lullaby/aiuaiu.aiff");
        reverb = Synth(\jpverb, [\in, aBus, \out, ~bus[\aiuaiu], \amp, -20.dbamp], fx);
        dry = Synth(\route, [\in, aBus, \out, ~bus[\aiuaiu], \amp, 0.dbamp], route);
        synth = Synth(\grbuf, [\out, aBus, \buf, buf], instr);

        s.sync;

        Pdef(\lullaGrain,
            Pbind (
                \type, \set,
                \id, synth.nodeID,
                \args, #[\trig, \amp, \pan, \pos, \rate, \grainsize],
                \dur, 20.reciprocal,
                // \dur, 200.reciprocal,
                \reverb, Pfunc({ reverb.set(\amp, rrand(-70, -20).dbamp)}),
                \trig, Pseq([1, 0], inf),
                \db, -12,
                \pos, Pseg([0.0, 1.0], buf.duration, \lin, inf),
                \rate, Prand((-1, -0.9..1).midiratio, inf),
                \pan, Pwhite(-1, 1),
                \grainsize, Pwhite(0.01, 0.5),
                // \grainsize, Pwhite(0.001, 0.05),
            )
        ).play;

        ~free[\aiuaiu] = {
            fork {
                var releaseTime = 5;
                Pdef(\lullaGrain).stop;
                1.wait;
                Pdef(\lullaGrain).clear;
                dry.release(releaseTime);
                reverb.release(releaseTime);
                synth.release(releaseTime);
                releaseTime.wait;
                group.free;
            }.play;
        };
    }
)

// ~free[\aiuaiu].();
