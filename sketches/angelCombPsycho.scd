( 
    ~angelCombPsychoFx = List.new(0);
    1.do{
        ~angelCombPsychoFx.add(
            FxChain.new(
                fadeInTime: 5,
                level: -9.dbamp,
                fadeOutTime: 5,
                out: ~masterBus,
            );
        );
    };
    ~angelCombPsychoFx[0].addPar(
        \comb, [\mix, 0.4, \delay, 0.002, \decay, 4, \amp, -26.dbamp],
        \comb, [\mix, 0.4, \delay, 0.005, \decay, 4, \amp, -20.dbamp],
        \comb, [\mix, 0.4, \delay, 0.007, \decay, 4, \amp, -12.dbamp],
    ); 
    ~angelCombPsychoFx[0].add(\eq, [
        \hishelffreq, 600,
        \hishelfdb, -12,
        \locut, 120
    ]); 
    //--------------------------------------------------------- 

    ~angelCombPsycho = ~mb.collect{|id, idx|
        MBDeltaTrig.new(
            speedlim: 0.5, 
            threshold: 0.02,
            minibeeID: id,
            minAmp: -6,
            maxAmp: 6,
            function: {|dt, minAmp, maxAmp|
                var startPos, currentFx, buf, numFrames, 
                grainsize, numGrains, step, len, pos, rate, duration, 
                attack, release, legato;
                buf = ~buf[\angelKlli].choose;
                numFrames = buf.numFrames;
                grainsize = 0.2;
                numGrains = 20;
                step = grainsize * s.sampleRate;
                //in samples, not seconds....
                len = dt.linlin(0.0, 1.0, step, step * numGrains);
                startPos = rrand(0, numFrames - len);
                pos = (startPos, startPos+step..startPos+len); 
                startPos = pos[pos.size-1].mod(numFrames); 
                rate = 1;
                duration = len/s.sampleRate;
                attack = rrand(0.1, 0.2);
                release = duration - attack;
                legato = 2;

                Pbind(
                    \instrument, \playbuf,
                    \buf, buf,
                    \dur, grainsize,
                    \attack, Pkey(\dur),
                    \release, Pkey(\dur) * 2,
                    \startPos, Pseq(pos),
                    \legato, legato,
                    \rate, Pwhite(0.99, 1.01),
                    \db, Pseg([-70, 0, -70], [attack, release]), 
                    \pan, Pwhite(-1.0, 1.0),
                    \out, Pfunc({
                        currentFx = ~angelCombPsychoFx.choose;
                        currentFx.in
                    }),
                    \group, Pfunc({ currentFx.group }),
                ).play; 
            }
        );
    }; 
) 
// (
//     // s.record("/home/kf/sc/100-sketches/videos/audio/angelCombPsycho" ++ Date.getDate.stamp ++".wav");
//     ~angelCombPsychoFx.do(_.play);
//     ~angelCombPsycho.do(_.play);
// )
// (
//     ~angelCombPsychoFx.do(_.free);
//     ~angelCombPsycho.do(_.free);
//     // SystemClock.sched(10, {s.stopRecording});
// ) 
