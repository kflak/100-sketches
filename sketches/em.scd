(
~emFx = FxChain.new(
    fadeInTime: 10, 
    level: -3.dbamp, 
    fadeOutTime: 10, 
    out: ~masterBus
); 
~emFx.add(\greyhole, [
    \mix, 0.6,
    \feedback, 0.6,
    \delayTime, 0.2
]); 
~emFx.add(\compressor, [
    \thresh, -4.dbamp,
    \ratio, 8,
    \amp, 3.dbamp,
]); 
~emFx.add(\jpverb, [
    \revtime, 2,
    \mix, 0.2
]);
~emFx.add(\eq, [
    \hishelfdb, -9,
    \hishelffreq, 500,
    \locut, 80
]);

~em = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.07,
        minibeeID: id,
        minAmp: -6,
        maxAmp: 6,
        function: {|dt, minAmp, maxAmp|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(~buf[\em]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.8,
                \release, Pkey(\dur) * 2,  
                \rate, Pwhite(0.25, 1),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 40),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, ~emFx.in,
                \group, ~emFx.group,
            ).play;
        }
    );
}; 
);

// s.record("~/sc-rec/em"+/+Date.localtime.stamp++".wav");
// (
// ~emFx.play;
// ~em.do(_.play);
// )
// (
// ~emFx.free;
// ~em.do(_.free);
// )
// s.stopRecording;
