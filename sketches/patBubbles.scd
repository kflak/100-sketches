( 
    ~patBubblesFx = FxChain.new(
        fadeInTime: 30,
        level: -6.dbamp,
        fadeOutTime: 30,
        out: ~masterBus,
    );

    ~patBubblesFx.add(\eq, [
        \locut, 400
    ]);

    ~patBubblesFx.add(\jpverb, [
        \revtime, 1, 
        \mix, 0.4,
    ]);

    ~patBubblesFx.play;

    ~patBubbles = Pbind(
        \instrument, \playbuf,
        \buf, Prand(~buf[\bubbles], inf),
        \attack, 0,
        \rate, 2,
        \dur, 1/8,
        \release, 0.1,
        \legato, 0.1,
        \pan, Pwhite(-1.0, 1.0),
        \db, Pwhite(-24, -18),
        \out, ~patBubblesFx.in,
        \group, ~patBubblesFx.group
    ).play; 
)

