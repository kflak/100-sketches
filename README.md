# 100 Sketches

SuperCollider code for the sound score of the contemporary dance research project [100 Sketches](http://roosnaflak.com/100-sketches/) by Norwegian/Estonian choreographer duo Külli Roosna and Kenneth Flak. 

## Dependencies
- SC3-plugins
- JPVerb
- pydoncli.py (for connecting Minibees)
- ddwPatterns.quark
- https://gitlab.com/kflak/supercollider-quarks.git
## Sensors

In the research we use [SenseStage](https://sensestage.eu) MiniBee accelerometers attached to the performers' wrists to trigger and/or control parts of the sound score.

## Authors
Kenneth Flak - Külli Roosna  
[Roosna & Flak](https://roosnaflak.com) 

## Supported by
[Norwegian Arts Council](https://www.kulturradet.no/)
