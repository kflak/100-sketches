(
    ~cuelist = CuePlayer.new();

    ~cuelist.add({
        ~startingTheShow = Routine {
            "starting".postln;
            this.executeFile(~sketchdir+/+"klliMicBreath.scd");
            300.wait;
            "water starts".postln;
            60.wait;
            "kenneth solo sounds on".postln;
            ~klliMicBreath[\fx].free;
            ~klliMicBreath[\water].stop;
            ~klliMicBreath[\osc].clear;
            ~klliMicBreath[\amp].free;
            ~klliMicBreath[\timeline].stop; 
            "breathulator".postln;
            this.executeFile(~sketchdir+/+"breathulator.scd");
            ~breathulatorFx.level = -12.dbamp;
            ~breathulatorFx.fadeInTime = 45;
            ~breathulatorFx.fadeOutTime = 95;
            ~breathulatorFx.play;
            ~breathulator[4..7].do(_.play); 
            60.wait;
            "trumpetulator".postln;
            this.executeFile(~sketchdir+/+"trumpetulator.scd");
            ~trumpetulatorFx.level = -6.dbamp;
            ~trumpetulatorFx.fadeInTime = 240;
            ~trumpetulatorFx.fadeOutTime = 120;
            ~trumpetulatorFx.play;
            ~trumpetulator[4..7].do(_.play); 
            90.wait; 
            "breathulator free".postln;
            ~breathulatorFx.free; 
            90.wait; 
            ~breathulator[4..7].do(_.free); 
            "metals".postln;
            this.executeFile(~sketchdir+/+"metals.scd"); 
            ~metalsFx.do(_.play);
            ~metals[4..5].do(_.play);
            45.wait; 
            "cellulator".postln;
            this.executeFile(~sketchdir+/+"cellulator.scd");
            ~cellulatorFx.do(_.play);
            ~cellulator[6..7].do(_.play); 
        }.play;
    }); 

    ~cuelist.add({
        ~kulliEnter = Routine{
            "corona focus, Külli enter".postln;
            this.executeFile(~sketchdir+/+"coronaWhisperGrain.scd");
            ~coronaWhisperGrainFx.do(_.fadeOutTime_(60));
            45.wait;
            ~coronaWhisperGrainFx.do(_.play);
            ~coronaWhisperGrain[1..3].do(_.play); 
            60.wait;
            this.executeFile(~sketchdir+/+"focus.scd");
            ~focusFx.do(_.play);
            ~focus[0].play; 
            ~trumpetulatorFx.free;
            ~trumpetulator[4..7].do(_.free); 
        }.play;
    }); 

    ~cuelist.add({
        ~whisperToThinkingSpace = Routine{
            "kenneth exit, kulli whisper solo".postln;
            ~cellulatorFx.do(_.free);
            ~cellulator[6..7].do(_.free); 
            ~metalsFx.do(_.free);
            ~metals[4..5].do(_.free); 
            // 90.wait; 
            this.executeFile(~sketchdir+/+"mbLooper.scd");
            120.wait;
            ~coronaWhisperGrainFx.do(_.free);
            ~coronaWhisperGrain[1..3].do(_.free); 
            ~focusFx.do(_.free);
            ~focus[0].free; 
        }.play;
    });

    ~cuelist.add({
        ~duets = Routine{
            "kenneth enter into duet".postln;
            30.wait;
            this.executeFile(~sketchdir+/+"mbFM.scd");
            ~mbFMFx.play;
            ~mbFM.do(_.play);
            45.wait;
            ~mbLooperFx.free;
            ~mbLooper[0..3].do(_.free); 
            75.wait; 
            this.executeFile(~sketchdir+/+"mbEarthulator.scd");
            ~mbEarthulatorFx.level = -26.dbamp;
            ~mbEarthulatorFx.fadeInTime = 20;
            ~mbEarthulatorFx.fadeOutTime = 40; 
            ~mbEarthulatorFx.play;
            ~mbEarthulator.do(_.play); 
            40.wait;
            this.executeFile(~sketchdir+/+"angelReverb.scd");
            ~angelReverbFx.do(_.fadeInTime(45));
            ~angelReverbFx.do(_.fadeOutTime(60));
            ~angelReverbFx.do(_.level( -60.dbamp ));
            ~angelReverbFx.do(_.play);
            ~angelReverb[0].play; 
            ~mbEarthulatorFx.free;
            ~mbEarthulator.do(_.free);
            80.wait;
            ~angelReverbFx.do(_.free);
            ~angelReverb[0..1].do(_.free); 
            this.executeFile(~sketchdir+/+"mbEarthulator.scd");
            ~mbEarthulatorFx.level = 0.dbamp;
            ~mbEarthulatorFx.fadeInTime = 30;
            ~mbEarthulatorFx.fadeOutTime = 90; 
            ~mbEarthulatorFx.play;
            ~mbEarthulator.do(_.play); 
            60.wait;
            ~mbFMFx.free;
            ~mbFM.do(_.free);
            90.wait;
            ~mbEarthulatorFx.free;
            "stomach duet".postln;
            this.executeFile(~sketchdir+/+"mbLeavulator.scd");
            ~mbLeavulatorFx.do(_.fadeOutTime_(60));
            ~mbLeavulatorFx.do(_.play);
            ~mbLeavulator.do(_.play); 
            90.wait;
            ~mbEarthulator.do(_.free); 
            170.wait;
            "birdgirl".postln;
            this.executeFile(~sketchdir+/+"birdGirl.scd");
            ~birdGirlFx.do(_.fadeInTime_(150));
            ~birdGirlFx.do(_.level_(0.dbamp));
            ~birdGirlFx.do(_.play);
            ~birdGirl[0..1].do(_.play); 
            ~mbLeavulator[0..1].do(_.free); 
            120.wait;
            ~mbLeavulatorFx.do(_.free);
            60.wait;
            ~mbLeavulator[2..7].do(_.free);
        }.play;   
    }); 

    ~cuelist.add({
        ~doubling = Routine {
            "doubling happening".postln;
            ~birdGirlFx.do(_.free);
            ~birdGirl[0..1].do(_.free); 
            60.wait;
            this.executeFile(~sketchdir+/+"unisonoBubbles.scd");
            ~unisonoBubblesFx.play;
            ~unisonoBubbles[2..3].do(_.play); 
            ~unisonoBubbles[6..7].do(_.play); 
            this.executeFile(~sketchdir+/+"patBubbles.scd");
            70.wait;
            this.executeFile(~sketchdir+/+"mbShh.scd");
            ~mbShhFx.play;
            ~mbShh[0].play;
            ~mbShh[4].play;
            30.wait;
            this.executeFile(~sketchdir+/+"numbers.scd");
            ~numbersFx.level = -12.dbamp;
            ~numbersFx.fadeInTime = 60;
            ~numbersFx.fadeInTime = 90;
            ~numbersFx.play;
            ~numbers[5].play;
            this.executeFile(~sketchdir+/+"talkulator.scd");
            ~talkulatorFx.play;
            ~talkulator[1].play;
            10.wait;
            this.executeFile(~sketchdir+/+"bd.scd");
            ~bdFx.play;
            ~bd[2].play;
            ~bd[6].play; 
        }.play;
    });

    ~cuelist.add({
        ~micSwing = Routine{
            "micswing".postln;
            ~patBubblesFx.free;
            ~patBubbles.stop;
            ~unisonoBubblesFx.free;
            ~unisonoBubbles[2..3].do(_.free);
            ~unisonoBubbles[6..7].do(_.free); 
            ~talkulatorFx.free;
            ~talkulator[1].free;
            ~bdFx.free;
            ~bd[2].free;
            ~bd[6].free; 
            ~numbersFx.free;
            ~numbers[5].free;
            20.wait;
            this.executeFile(~sketchdir+/+"numberCircleMic.scd")
        }.play;
    });

    ~cuelist.add({
        ~piano = Routine{
            var xTime = 90;
            ~free[\numberCircleMic].();
            ~dry.set(\gate, 0);
            "arvo".postln;
            this.executeFile(~sketchdir+/+"arvo2.scd");
            ~arvo2Fx.fadeOutTime = xTime;
            ~arvo2Fx.play;
            ~arvo2[0..3].do(_.play); 
            120.wait;
            this.executeFile(~sketchdir+/+"arvoLow.scd");
            ~arvoLowFx.fadeInTime = xTime;
            ~arvoLowFx.fadeOutTime = 120;
            ~arvoLowFx.play;
            ~arvoLow[0..3].do(_.play); 
            240.wait;
            ~arvo2[0..3].do(_.free); 
            "arvodist starts".postln;
            ~arvoLowFx.free;
            this.executeFile(~sketchdir+/+"arvoDist.scd");
            ~arvoDistFx.fadeInTime = xTime;
            ~arvoDistFx.fadeOutTime = xTime;
            ~arvoDistFx.play;
            ~arvoDist[0..3].do(_.play); 
            xTime.wait;
            ~arvoLow[0..3].do(_.free); 
        }.play;
    });

    ~cuelist.add({
        ~kennethStopsKlli = Routine{ 
            "kenneth stops Klli".postln;
            15.wait;
            ~arvoDistFx.free;
            30.wait;
            ~arvoDist[0..3].do(_.free); 
            this.executeFile(~sketchdir+/+"runningWater.scd");
            ~runningWaterFx.play;
            ~runningWater[4..7].do(_.play);
            this.executeFile(~sketchdir+/+"velcroClean.scd");
            ~velcroCleanFx.play;
            ~velcroClean[0..3].do(_.play);
            30.wait;
            this.executeFile(~sketchdir+/+"micLongReverb.scd");
        }.play;
    });

    ~cuelist.add({
        ~kllifourmics = Routine{
            "külli four mic meditation".postln;
            this.executeFile(~sketchdir+/+"stormEye.scd");
            this.executeFile(~sketchdir+/+"stormEyeSingleChain.scd");
            this.executeFile(~sketchdir+/+"stormEyeSingleChain1.scd");
            this.executeFile(~sketchdir+/+"stormEyeSingleChain2.scd");
        }.play;
    });

    ~cuelist.add({
        "free all mics".postln;
        ~stormEye[\free].();
        ~stormEyeSingleChain[\free].();
        ~stormEyeSingleChain1[\free].();
        ~stormEyeSingleChain2[\free].();
        ~micLongReverbFx.free;
    });

    //touchOsc as cue trigger
    OSCdef(\cue, {~cuelist.next}, "/1/cue");

    // LaunchPad
    if(~hasLaunchPad){
        LPButton.new(8, onFunc: {~cuelist.next});
    };

    // ~cuelist.gui(
    //     monitorInChannels: s.options.numInputBusChannels,
    //     monitorOutChannels: s.options.numOutputBusChannels,
    //     options:(
    //         shortcuts:true,
    //         metronome: false,
    //     )
    // ); 
)
