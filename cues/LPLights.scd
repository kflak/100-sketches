(
var activeLight;
var lightValues = ~dmx.currentCue.data;
var padColors = List.new;
var colors = [[\redhi, \redlo], [\greenhi, \greenlo], [\amberhi, \amberlo]];
var cues = DMXSubCue.new ! 512; 
var page, pad, sideButtons, sideButtonValues;

~allLights.do{|i, idx|
    i.do{|j, jdx|
        padColors.add(colors[idx.mod(colors.size)])
    }
};

page = LPPage.new;
pad = ~allLights.flatten.clump(8).collect{|i, idx| 
    i.do{|j, jdx|
        page.addPad(idx, jdx, {
            activeLight = j;
        },{
            activeLight = nil;
        }, 
        false, padColors[idx][0], padColors[idx][1])
    }
};

sideButtonValues = [0.2, 0.1, 0.05, 0.01, -0.01, -0.05, -0.1, -0.2];
sideButtonColors = [\yellow, \amberhi, \amberhi, \amberlo, \amberlo, \amberhi, \amberhi, \yellow];
sideButtons = sideButtonValues.do{|val, idx|
    page.addSide(idx, 
        onFunc: {
            activeLight !? {
                lightValues[activeLight] = ~dmx.currentCue.data[activeLight] + sideButtonValues[idx];
                lightValues[activeLight] = lightValues[activeLight].clip(0.0, 1.0);
                cues[activeLight] = lightValues[activeLight];
                ~dmx.fade(cues[activeLight], 0.05);
            }
        }, 
        onColor: \greenhi,
        offColor: sideButtonColors[idx],
    );
}
)
