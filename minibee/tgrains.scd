(
    ~lullaTGrainStop.value();
    fork{ 
        var buf =  Buffer.readChannel(s, ~root+/+"audio/lullaby/aiuaiu.aiff", channels: 0);
        var revBus = Bus.audio(s, ~numSpeakers);
        var aBus = Bus.audio(s, ~numSpeakers);
        var reverb = Synth(\jpverb, [\revtime, 6, \in, revBus, \out, 0]);
        var route = [
            Synth(\route, [\in, aBus, \out, 0]),
            Synth(\route, [\in, aBus, \out, revBus, \amp, 0.7])
        ];
        var window = Buffer.loadCollection(s, 
            1024.collect({|i| i.linexp(0, 1024, 0.0000001, 1)})
        );
        // s.sync;

        Pdef(\lullaTGrains,
            Pmono(
                \tgrains,
                \buf, buf,
                // \dur, Pexprand(0.1, 3), 
                \dur, 1/16,
                // \dur, Prand([1/16, 1/8], inf),
                \trig, Pseq([1, 0], inf),
                \amp, 0.dbamp,
                \pos, Pseg(Pseq([0, 1], inf), Pseq([b.duration], inf), \linear),
                // \pos, Pwhite(0.0, 1.0),
                \pan, Pwhite(-0.5, 0.5),
                \window, b,
                \reverb, Pfunc({route[1].set(\amp, rrand(-70, -0).dbamp)}),
                \grainsize, Pkey(\dur) * Pwhite(1, 6), 
                // \grainsize, Pn(Pkey(\dur) * Pseg([0.01, 0.5], 20)),
                \graindecay, Pkey(\grainsize) * 0.5,
                // \graindecay, 0,
                \grainattack, 0,
                // \grainattack, Pkey(\grainsize) * 0.1,
                \out, aBus,
            )
        ).play;

        ~lullaTGrainStop = {
            fork{
                Pdef(\lullaTGrains).stop;
                buf.free;
                [revBus, aBus, reverb].do(_.free);
                route.do(_.free);
            }
        };
    }
) 

~lullaTGrainStop.value();


~lullaTGrainStop.value();
s.plotTree;
