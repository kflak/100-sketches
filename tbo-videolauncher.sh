#!/bin/bash

monitor=("DP-1" "HDMI-2")

for i in {0..1}; do
    if bspc query --monitors --names | grep ${monitor[$i]}; then
        bspc monitor ${monitor[$i]} --focus
    fi
    mpv --really-quiet --no-audio $HOME/Videos/tbo/tbo-sun1.m4v &
    sleep 4
done 
