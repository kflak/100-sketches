(
    ~presentation = Routine({ 
        this.executeFile(~sketchdir+/+"arvopbind.scd");
        this.executeFile(~sketchdir+/+"trumpetulator.scd");
        this.executeFile(~sketchdir+/+"mbEarthulator.scd");

        s.sync;

        "running".postln;

        ~arvopbindFx.play;
        ~arvopbind.do({|i, idx| if(idx<4){i.play}});

        // 10.wait;
        80.wait;

        "trumpet".postln;

        ~trumpetulatorFx.play;
        ~trumpetulator.do({|i, idx| if(idx>3){i.play}});

        5.wait;

        ~arvopbindFx.free;
        ~arvopbind.do(_.free);

        60.wait;

        ~trumpetulatorFx.free;
        ~trumpetulator.do(_.free);

        "earth".postln;

        8.wait;

        ~mbEarthulatorFx.play;
        ~mbEarthulator.do(_.play);

        60.wait;

        ~mbEarthulatorFx.free;
        ~mbEarthulator.do(_.free);

    }).play;
)

(
    fork{

        this.executeFile(~sketchdir+/+"arvo2.scd");
        this.executeFile(~sketchdir+/+"arvopbind.scd");
        this.executeFile(~sketchdir+/+"bd.scd");
        this.executeFile(~sketchdir+/+"dirtdelta.scd");
        this.executeFile(~sketchdir+/+"mbArpeggiator.scd");
        this.executeFile(~sketchdir+/+"mbBellulator.scd");
        this.executeFile(~sketchdir+/+"mbCello.scd");
        this.executeFile(~sketchdir+/+"mbEarthulator.scd");
        this.executeFile(~sketchdir+/+"mbHallelujah.scd");
        this.executeFile(~sketchdir+/+"mbLeavulator.scd");
        this.executeFile(~sketchdir+/+"mbShh.scd");
        this.executeFile(~sketchdir+/+"mbTwinkle.scd");
        this.executeFile(~sketchdir+/+"mbTwinkleFB.scd");
        this.executeFile(~sketchdir+/+"trumpetulator.scd");
        this.executeFile(~sketchdir+/+"unisonoBubbles.scd"); 
        this.executeFile(~sketchdir+/+"sn.scd");
        this.executeFile(~sketchdir+/+"hh.scd");
        this.executeFile(~sketchdir+/+"mbTreeperculator.scd");
        this.executeFile(~sketchdir+/+"mbFM.scd");
        this.executeFile(~sketchdir+/+"mbWalkGravel.scd");
        this.executeFile(~sketchdir+/+"brPiano.scd");

        s.sync;

        //------------------------------------------------------------------------- 
        // Page 0

        ~lp.add(LPPage.new);

        ~lp.last.addTop(0, {}, {}, false, \amberhi, \amberlo);
        ~lp.last.addTop(1, {}, {LPPage.next}, false, \amberhi, \amberlo);

        8.do{|i| ~lp.last.addPad(0, i, {~mbCello[i].play}, {~mbCello[i].stop}, true, \redhi, \redlo)};
        ~lp.last.addSide(0, {~mbCelloFx.do(_.play)}, {~mbCelloFx.do(_.stop)}, true, \redhi, \redlo); 

        8.do{|i| ~lp.last.addPad(1, i, {~mbBellulator[i].play}, {~mbBellulator[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(1, {~mbBellulatorFx.play}, {~mbBellulatorFx.stop}, true, \amberhi, \amberlo);

        8.do{|i| ~lp.last.addPad(2, i, {~bd[i].play}, {~bd[i].stop}, true, \greenhi, \greenlo)};
        ~lp.last.addSide(2, {~bdFx.play}, {~bdFx.stop}, true, \greenhi, \greenlo);

        8.do{|i| ~lp.last.addPad(3, i, {~dirtdelta[i].play}, {~dirtdelta[i].stop}, true, \redhi, \redlo)};
        ~lp.last.addSide(3, {~dirtdeltaFx.play}, {~dirtdeltaFx.stop}, true, \redhi, \redlo);

        8.do{|i| ~lp.last.addPad(4, i, {~mbTwinkle[i].play}, {~mbTwinkle[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(4, {~mbTwinkleFx.play}, {~mbTwinkleFx.stop}, true, \amberhi, \amberlo);

        8.do{|i| ~lp.last.addPad(5, i, {~mbShh[i].play}, {~mbShh[i].stop}, true, \greenhi, \greenlo)};
        ~lp.last.addSide(5, {~mbShhFx.play}, {~mbShhFx.stop}, true, \greenhi, \greenlo);

        8.do{|i| ~lp.last.addPad(6, i, {~mbArpeggiator[i].play}, {~mbArpeggiator[i].stop}, true, \redhi, \redlo)};
        ~lp.last.addSide(6, {~mbArpeggiatorFx.play}, {~mbArpeggiatorFx.stop}, true, \redhi, \redlo);

        8.do{|i| ~lp.last.addPad(7, i, {~unisonoBubbles[i].play}, {~unisonoBubbles[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(7, {~unisonoBubblesFx.play}, {~unisonoBubblesFx.stop}, true, \amberhi, \amberlo);

        s.sync;

        //------------------------------------------------------------------------- 
        // Page 1

        ~lp.add(LPPage.new);

        ~lp.last.addTop(0, {}, {LPPage.prev}, false, \amberhi, \amberlo);
        ~lp.last.addTop(1, {}, {LPPage.next}, false, \amberhi, \amberlo);

        //0 Hallellujah
        8.do{|i| ~lp.last.addPad(0, i, {~mbHallelujah[i].play}, {~mbHallelujah[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(0, {~mbHallelujahFx.do(_.play)}, {~mbHallelujahFx.do(_.stop)}, true, \amberhi, \amberlo);

        //1 flutterSpeak / birdies
        8.do{|i| ~lp.last.addPad(1, i, {~mbFlutterSpeak[i].play}, {~mbFlutterSpeak[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(1, {
            this.executeFile(~sketchdir+/+"mbFlutterSpeak.scd"); 
            ~mbFlutterSpeakFx.do(_.play)
        }, {
            ~mbFlutterSpeakFx.do(_.stop)
        }, true, \amberhi, \amberlo);

        //2 twinkleFB
        8.do{|i| ~lp.last.addPad(2, i, {~mbTwinkleFB[i].play}, {~mbTwinkleFB[i].stop}, true, \greenhi, \greenlo)};
        ~lp.last.addSide(2, {~mbTwinkleFBFx.do(_.play)}, {~mbTwinkleFBFx.do(_.stop)}, true, \greenhi, \greenlo);

        //3 mbArpeggiator
        8.do{|i| ~lp.last.addPad(3, i, {~mbArpeggiator[i].play}, {~mbArpeggiator[i].stop}, true, \redhi, \redlo)};
        ~lp.last.addSide(3, {~mbArpeggiatorFx.play}, {~mbArpeggiatorFx.stop}, true, \redhi, \redlo);

        //4 leavulator / inside the stomach
        8.do{|i| ~lp.last.addPad(4, i, {~mbLeavulator[i].play}, {~mbLeavulator[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(4, {~mbLeavulatorFx.do(_.play)}, {~mbLeavulatorFx.do(_.stop)}, true, \amberhi, \amberlo);

        //5 mbTwinkle
        8.do{|i| ~lp.last.addPad(5, i, {~mbTwinkle[i].play}, {~mbTwinkle[i].stop}, true, \greenhi, \greenlo)};
        ~lp.last.addSide(5, {~mbTwinkleFx.do(_.play)}, {~mbTwinkleFx.do(_.stop)}, true, \greenhi, \greenlo);

        //6 trumpetulator
        8.do{|i| ~lp.last.addPad(6, i, {~trumpetulator[i].play}, {~trumpetulator[i].stop}, true, \redhi, \redlo)};
        ~lp.last.addSide(6, {~trumpetulatorFx.play}, {~trumpetulatorFx.stop}, true, \redhi, \redlo);

        //7 arvo2
        8.do{|i| ~lp.last.addPad(7, i, {~arvo2[i].play}, {~arvo2[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(7, {~arvo2Fx.play}, {~arvo2Fx.stop}, true, \amberhi, \amberlo); 

        s.sync;
        //---------------------------------------------------------------------------
        // Page 2

        ~lp.add(LPPage.new);

        ~lp.last.addTop(0, {}, {LPPage.prev}, false, \amberhi, \amberlo);
        ~lp.last.addTop(1, {}, {}, false, \amberhi, \amberlo);

        //0 Earthulator
        8.do{|i| ~lp.last.addPad(0, i, {~mbEarthulator[i].play}, {~mbEarthulator[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(0, {~mbEarthulatorFx.play}, {~mbEarthulatorFx.stop}, true, \amberhi, \amberlo);

        //1 snare drum
        8.do{|i| ~lp.last.addPad(1, i, {~sn[i].play}, {~sn[i].stop}, true, \greenhi, \greenlo)};
        ~lp.last.addSide(1, {~snFx.play}, {~snFx.stop}, true, \greenhi, \greenlo);

        //2 hihat
        8.do{|i| ~lp.last.addPad(2, i, {~hh[i].play}, {~hh[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(2, {~hhFx.play}, {~hhFx.stop}, true, \amberhi, \amberlo);

        //3 treeperculator
        8.do{|i| ~lp.last.addPad(3, i, {~mbTreeperculator[i].play}, {~mbTreeperculator[i].stop}, true, \greenhi, \greenlo)};
        ~lp.last.addSide(3, {~mbTreeperculatorFx.play}, {~mbTreeperculatorFx.stop}, true, \greenhi, \greenlo);

        //4 arvo
        8.do{|i| ~lp.last.addPad(4, i, {~arvopbind[i].play}, {~arvopbind[i].stop}, true, \redhi, \redlo)};
        ~lp.last.addSide(4, {~arvopbindFx.play}, {~arvopbindFx.stop}, true, \redhi, \redlo);

        //5 mbFM
        8.do{|i| ~lp.last.addPad(5, i, {~mbFM[i].play}, {~mbFM[i].stop}, true, \amberhi, \amberlo)};
        ~lp.last.addSide(5, {~mbFMFx.play}, {~mbFMFx.stop}, true, \amberhi, \amberlo);

        //6 walkgravel
        // 8.do{|i| ~lp.last.addPad(6, i, {~mbWalkGravel[i].play}, {~mbWalkGravel[i].stop}, true, \redhi, \redlo)};
        // ~lp.last.addSide(6, {~mbWalkGravelFx.play}, {~mbWalkGravelFx.stop}, true, \redhi, \redlo);

        //7 broken piano
        8.do{|i| ~lp.last.addPad(7, i, {~brPiano[i].play}, {~brPiano[i].stop}, true, \greenhi, \greenlo)};
        ~lp.last.addSide(7, {~brPianoFx.play}, {~brPianoFx.stop}, true, \greenhi, \greenlo);

    } 
) 

~mbWalkGravelFx.play;
~mbWalkGravel[4].play;
s.meter;
s.plotTree;
~lp.do(_.free)
~lp[0].freePad(0, 0);
~lp[0].free;
~lp[1].free;
~lp[2].free;

// blind:
arvo2 + trumpetulator
page 1 row 7 + page 1 row 6

leavulator
page 1 row 4

broken piano
page 2 row 7
