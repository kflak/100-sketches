#!/bin/bash 

shopt -s nullglob
videoarray=($1/*)
shuffledArray=( $(shuf -e "${videoarray[@]}") )
minlength=10
maxlength=40
monitor=$2

while true 
do
    for video in ${shuffledArray[*]}
    do
        starttime=2
        endtime=$(shuf -i $minlength-$maxlength)
        let duration=$endtime-$starttime
        if bspc query --monitors --names | grep $monitor; then
            bspc monitor $monitor --focus
        fi
        mpv --really-quiet --volume=30 --start=$starttime --end=$endtime $video
    done
done  
