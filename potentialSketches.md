# Potential sketches and their usage

## Tree
mbFM (brass-like sounds, works well together with mbWalkGravel)
mbWalkGravel
mbShh
mbTreeperculator
stormulator (into the depths kind of a thing, distorted whispers)

## Balancing Act on Water - on a stone, very still
focus Külli no 3

## Sand - Thinking Space On the Knees 
mbBreath (granular, semi-percussive breath sounds) Külli no 1

## Water
mbArpeggiator
mbFlutter Külli no 4
mbFM (brass-like sounds)
mbFMdrone (steppy)
mbSineFB
metals
runningWater (clean water sound, no processing)
unisonoBubbles

## Grassy/bushy something
mbLeavulator
stormulator (into the depths kind of a thing, distorted whispers)
velcrolator

## In the darkness, maybe in the water between high grass
mbScary

## Close-up handstand
mbTwinkle
trumpetulator

## Forresty
coronaWhisperGrain (elvish whispers) Külli no 2
rainRingModGrain
mbFlutter (fast, squirrely)

## General background sound
rainulator

## Other stuff
velcroClean






## Deprecated, needs rewriting
mbTalkulator
guitardeltasingle
guitarDreams
fmDirection
sea
seaForest
